import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JButton;


public class GUI {
	
	BufferedReader in;
    PrintWriter out;

	private JFrame getName_okno;
	private JTextField wiadomosc_text;
	private JTextArea dostepni_text;
	private JTextArea chat_text = new JTextArea();

	/**
	 * Launch the application.
	 * @throws IOException 
	 */
	
	public static void main(String[] args) throws IOException {
		GUI okno = new GUI();
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {					
					okno.getName_okno.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	    okno.run();
	}
	
	
	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
		
		wiadomosc_text.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e) 
            {
                out.println(wiadomosc_text.getText());
                wiadomosc_text.setText("");
            }
        });
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		getName_okno = new JFrame();
		getName_okno.setTitle("Chat!");
		getName_okno.setBounds(100, 100, 772, 507);
		getName_okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getName_okno.getContentPane().setLayout(null);
		
		wiadomosc_text = new JTextField();
		wiadomosc_text.setEditable(false);
		wiadomosc_text.setBounds(110, 349, 413, 84);
		getName_okno.getContentPane().add(wiadomosc_text);
		wiadomosc_text.setColumns(10);
		chat_text.setEditable(false);
		
		
		chat_text.setBounds(110, 53, 413, 259);
		getName_okno.getContentPane().add(chat_text);
		
		dostepni_text = new JTextArea();
		dostepni_text.setEditable(false);
		dostepni_text.setBounds(574, 53, 144, 259);
		getName_okno.getContentPane().add(dostepni_text);
		
		JButton wyslij_but = new JButton("Wy\u015Blij");
		wyslij_but.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				out.println(wiadomosc_text.getText());
                wiadomosc_text.setText("");
			}
		});
		wyslij_but.setBounds(533, 410, 89, 23);
		getName_okno.getContentPane().add(wyslij_but);
	}
	
    private String getName() 
    {
        return JOptionPane.showInputDialog(getName_okno,"Wybierz nick:","Wyb�r nicku!",JOptionPane.PLAIN_MESSAGE);
    }
    
    /*
     * Po��czenie z serverem o IP i porcie, nastepnie wejscie w petle w ktorej wszystko sie dzieje
     */
    public void run() throws IOException 
    {
        Socket socket = new Socket("0.0.0.0", 8081);
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);

        while (true) 
        {
            String linia = in.readLine();
            if (linia.startsWith("SUBMITNAME")) 
            {
                out.println(getName());
            } 
            else if (linia.startsWith("NAMEACCEPTED")) 
            {
                wiadomosc_text.setEditable(true);
            } 
            else if (linia.startsWith("nick")) 
            {
                dostepni_text.append(linia.substring(4) + "\n");
            }
            else if (linia.startsWith("wiadomosc"))
            {
            	chat_text.append(linia.substring(10)+"\n");
            }  
        }
    }
}
