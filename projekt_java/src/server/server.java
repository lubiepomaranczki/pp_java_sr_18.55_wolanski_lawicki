package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

import javax.swing.JOptionPane;


public class server 
{
	/*
	 * Port na kt�rym nas�uchuje serwer
	 */
    private static final int PORT = 8081;
    
    public static ArrayList<String>nicks = new ArrayList<String>();
    
    /*
     * Inicjalizacja obiektu Obsluga_XML odpowiedzialnego za stworzenie pliku, dodawanie go
     */
    static Obluga_XML temp = new Obluga_XML();
   
    /*
     * Zestaw string�w nazw u�ytkownik�w. Istnieje po to aby sprawdzi� czy dany nick jest wolny
     */
    private static HashSet<String> names = new HashSet<String>();

    /*
     * Zestaw printwriter�w potrzebny do dzielenia wiadomo�ci
     */
    private static HashSet<PrintWriter> writers = new HashSet<PrintWriter>();
    private static HashSet<PrintWriter> nicki = new HashSet<PrintWriter>();
    /*
     * Main nas�uchuj�cy na zadanym porcie. Tworz�cy dokumentacje XML
     */
    public static void main(String[] args) throws Exception 
    {
    	temp.stworzXML();
        System.out.println("Server dziala");
        ServerSocket listener = new ServerSocket(PORT);
        try {
            while (true) 
            {
                new Handler(listener.accept()).start();
            }
        } 
        finally 
        {
            listener.close();
        }
    }


    /*
     * Klasa Handler odpowiedzialna za obs�uge klienta oraz dystrybuowanie jego wiadomo�ci
     */
    private static class Handler extends Thread 
    {
        private String nick;
        private Socket socket;
        private BufferedReader in;
        private PrintWriter out;
        private PrintWriter nickOut;
        
        public Handler(Socket socket) 
        {
            this.socket = socket;
        }

        /*
         * Metoda run obs�uguje klienta. Po zapytaniu jaki nick czeka na unikalny, nie znajdujacy sie na serwerze.
         * Nastepnie rejestruje ciag przychodzacy i dzieli go z innymi uzytkownikami
         */
        public void run() 
        {        	
            try 
            {
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                out = new PrintWriter(socket.getOutputStream(), true);
                nickOut = new PrintWriter(socket.getOutputStream(), true);
                

                /*
                 * W petlni wykonuje sie zapytanie o nick tak dlugo dopoki uzytkownik nie poda unikalnego nicku
                 */
                while (true) 
                {
                    out.println("SUBMITNAME");
                    nick = in.readLine();
                    if (nick == null) 
                    {
                        return;
                    }
                    synchronized (names) 
                    {
                        if (!names.contains(nick)) 
                        {
                            names.add(nick);
                            break;
                        }
                    }
                }

                /*
                 * Po zalogowaniu klient jest dopuszczony do otrzymania nowych wiadomosci
                 */
                out.println("NAMEACCEPTED");
                writers.add(out);
                
                nickOut.println(nick);
                nicki.add(nickOut);
                
                for (PrintWriter nazwyUzy : nicki) 
                {	                 	
                	nazwyUzy.println("nick" + nick);
                }
                
                //godzina potrzebna do XML jak i do wy�wietlania na serwerze
                DateFormat df = new SimpleDateFormat("HH:mm");
            	Date dzis = Calendar.getInstance().getTime();        
            	String godzina = df.format(dzis);
                

                /*
                 * Otrzymuje wiadomosci od klienta. W przypadku jestli wystepuje konflikt ignoruje innych klient�w
                 */
                while (true) 
                {
                    String input = in.readLine();
                    if (input == null) 
                    {
                        return;
                    }
                    else
                    {
                    	//dodajemy do XML kolejne wiadomosci (trzy pola: nick, wiadomosc, godzina)
                        try 
                        {
							temp.dodajXmlNode(nick, input, godzina);
						} 
                        catch (Exception e) 
                        {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
                    }
                    /*
                     * Dzielimy sie wiadomosciami z innymi writerami
                     */
                    for (PrintWriter writer : writers) 
                    {	                 	
                    	writer.println("wiadomosc " + "["+godzina+"] " + nick + ": " + input);
                    }
                    /*
                    for (PrintWriter writer : writers) 
                    {	                 	
                    	writer.println("nick" + nick);
                    }*/
                }
            } 
            catch (IOException e) 
            {
                System.out.println(e);
            } 
            /*
             * Zamkniecie clienta i socketu. Wyczyszenie nicku klienta i jego wiadomosci
             */
            finally 
            {
                if (nick != null) 
                {
                    names.remove(nick);
                }
                if (out != null) 
                {
                    writers.remove(out);
                }
                
                /*
                 * Zamkniecie polaczenia
                 */
                try 
                {
                    socket.close();
                } 
                catch (IOException e) 
                {
                	
                }
            }
        }
    }
}