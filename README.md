Środa 18.55, Ławicki, Wolański

### Mini-projekt Java - chat ###

## Co działa ##
Momentu uruchomienia aplikacji i wybraniu sobie nicku, użytkownik widzi wszystkie nowe wiadomości. Nie ma dostępu do starych. Możliwość pisania jest tylko dostępna po przydzieleniu prawidłowego nicku.

Okno dostępnych użytkowników działa, ale trzeba je poprawić, gdyż wyświetla tylko użytkowników, którzy zalogowali się po nas. Trochę bez sensu, bo pierwszy użytkownik widzi wszystkich; ostatni tylko siebie

## Co nie działa? ##
Tak jak było pisane, nie do końca działa wyświetlanie dostępnych użytkowników.