package server;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

 /*
  * Klasa obslugujaca plik XML
  */
public class Obluga_XML
{
	
	public static final String xmlFilePath = "xmlfile.xml";
	
	/*
	 * Tworzymy XML zawierajacy wierzcholki: godzina, nick, wiadomosc
	 */
	public void stworzXML()
	{
		try
		{
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

			Document document = documentBuilder.newDocument();
			Element root = document.createElement("Chat");
		
			document.appendChild(root);
			
			Element godzina = document.createElement("godz");
			root.appendChild(godzina);
			
			Element nick = document.createElement("nick");
			root.appendChild(nick);
			
			Element wiadomosc = document.createElement("Wiadomosc");
			root.appendChild(wiadomosc);
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
		
			Transformer transformer = transformerFactory.newTransformer();
		
			DOMSource domSource = new DOMSource(document);
		
			StreamResult streamResult = new StreamResult(new File(xmlFilePath));
		
			transformer.transform(domSource, streamResult);


		System.out.println("Done creating XML File");
		}
		catch (ParserConfigurationException pce) 
		{
			pce.printStackTrace();
		}
		
		catch (TransformerException tfe) 
		{
	    	tfe.printStackTrace();
	    }
		
		
	}
	/*
	 * Funkcja przyjmuje trzy parametry od servera odnosnie wiadomosci jaka klient napisal,
	 * nastepnie tworzy nowe wierzcholki z danymi polami
	 */
	public void dodajXmlNode(String _nick, String _wiadomosc, String _godzina) throws Exception
	{
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		
		Document document = documentBuilder.parse(xmlFilePath);
		Element root = document.getDocumentElement();
		
		Element godzina = document.createElement("godz");
		godzina.appendChild(document.createTextNode(_godzina));
		root.appendChild(godzina);
		
		Element nick = document.createElement("nick");
		nick.appendChild(document.createTextNode(_nick));
		root.appendChild(nick);

		Element wiadomosc = document.createElement("wiadomosc");
		wiadomosc.appendChild(document.createTextNode(_wiadomosc));
		root.appendChild(wiadomosc);

		DOMSource source = new DOMSource(document);

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		StreamResult result = new StreamResult(xmlFilePath);
		transformer.transform(source, result);
	}
}
